#include<GL/glut.h>
#include<stdlib.h>
#include<windows.h>
#include<iostream>
#include<fstream>
#include "game.h"

#define ROWS 40.0
#define COLUMNS 60.0

std::ofstream ofile;
std::ifstream ifile;
bool game_over=false;
extern int direction;
int score=0;
int fps=170;

void init()
{
    glClearColor(1.0,1.0,1.0,0.0);
    initGrid(COLUMNS,ROWS);
}

void display()
{
    if(game_over)
    {
        ofile.open("score.dat",std::ios::trunc);
        ofile<<score<<std::endl;
        ofile.close();
        ifile.open("score.dat",std::ios::in);
        char a[4];
        ifile>>a;
        char text[20]= "Score: ";
        strcat(text,a);
        MessageBox(NULL,text,"Game Over",0);
        exit(0);
    }
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    draw_grid();
    draw_target();
    draw_snake();
    glutSwapBuffers();
}
void reshape_screen(int w, int h)
{
    glViewport(0,0,(GLfloat)w,GLfloat(h));
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0,COLUMNS,0.0,ROWS,-1.0,1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
void fps_control(int)
{
    glutPostRedisplay();
    glutTimerFunc(fps-score*5,fps_control,0);

}
void control(int input,int x,int y)
{
    switch(input)
    {
    case GLUT_KEY_UP:
        if(direction!=DOWN)
            direction=UP;
        break;
    case GLUT_KEY_DOWN:
        if(direction!=UP)
            direction=DOWN;
        break;
    case GLUT_KEY_RIGHT:
        if(direction!=LEFT)
            direction=RIGHT;
        break;
    case GLUT_KEY_LEFT:
        if(direction!=RIGHT)
            direction=LEFT;
        break;
    }
}

int main(int argc,char**argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutInitWindowPosition(10,10);
    glutInitWindowSize(1200,800);
    glutCreateWindow("Snake Game");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape_screen);
    glutSpecialFunc(control);
    glutTimerFunc(fps,fps_control,0);
    init();
    glutMainLoop();
    return 0;
}


