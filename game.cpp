#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include<stdlib.h>
#include <ctime>
#include <string.h>
#include "game.h"

int positionX[MAX+1]={8,7,6,5,4,3};
int positionY[MAX+1]={20,20,20,20,20,20};
int length=6;
bool flag = false;
extern int score;
extern bool game_over;
bool target=false;
int rows=0,columns=0;
int direction = RIGHT;
int targetx,targety;

void unit(int x,int y)
{
    glLoadIdentity();
    if(x==0||x==columns-1||y==0||y==rows-1)
    {
        glColor3f(0.9,0.0,0.0);

        glBegin(GL_POLYGON);
            glVertex2d(x,y); glVertex2d(x+1,y);
            glVertex2d(x+1,y); glVertex2d(x+1,y+1);
            glVertex2d(x+1,y+1); glVertex2d(x,y+1);
            glVertex2d(x,y+1); glVertex2d(x,y);
        glEnd();
    }
    else
    {
        glColor3f(0.96,0.96,0.96);
        //glColor3f(1.0,1.0,1.0);
        glLineWidth(0.1);

        glBegin(GL_LINES);
            glVertex2d(x,y); glVertex2d(x+1,y);
            glVertex2d(x+1,y); glVertex2d(x+1,y+1);
            glVertex2d(x+1,y+1); glVertex2d(x,y+1);
            glVertex2d(x,y+1); glVertex2d(x,y);
        glEnd();
    }

}
int random(int min,int max)
{
    if(!flag)
    {
        srand(time(NULL));
        flag=true;
    }
    else
        flag=false;
    return min+rand()%(max-min);
}


void initGrid(int x,int y)
{
    columns=x;
    rows=y;
}

void draw_grid()
{
    for(int i =0;i<columns;i++)
    {
        for(int j=0;j<rows;j++)
            {unit(i,j);}
    }
}

void draw_snake()
{
    for(int i =length-1;i>0;i--)
    {
        positionX[i]=positionX[i-1];
        positionY[i]=positionY[i-1];
    }
    for(int i=0;i<length;i++)
    {
        glColor3f(0.137255,0.137255,0.556863);
        if(i==0)
        {
            glColor3f(0.560784, 0.560784, 0.737255);
            switch(direction)
            {
            case UP:
                positionY[i]++;
                break;
            case DOWN:
                positionY[i]--;
                break;
            case RIGHT:
                positionX[i]++;
                break;
            case LEFT:
                positionX[i]--;
                break;
            }
            if(positionX[i]==1||positionX[i]==columns-2||positionY[i]==1||positionY[i]==rows-2)
                game_over=true;
            else if(positionX[i]==targetx && positionY[i]==targety)
            {
                target=false;
                score++;
                if(length<MAX)
                    length++;
                else
                    length=MAX;
            }
            for(int j=1;j<length;j++)
            {
                if(positionX[j]==positionX[0] && positionY[j]==positionY[0])
                    game_over=true;
            }
        }
        glBegin(GL_POLYGON);
            glVertex2d(positionX[i],positionY[i]);
            glVertex2d(positionX[i]+1,positionY[i]);
            glVertex2d(positionX[i]+1,positionY[i]+1);
            glVertex2d(positionX[i],positionY[i]+1);
        glEnd();
    }

}

void draw_target()
{
    if(!target)
    {
        targetx=random(1,columns-2);
        targety=random(1,rows-2);
        target=true;
    }
    glColor3f(0.0,0.0,0.0);
    glBegin(GL_POLYGON);
        glVertex2d(targetx,targety);
        glVertex2d(targetx+1,targety);
        glVertex2d(targetx+1,targety+1);
        glVertex2d(targetx,targety+1);
    glEnd();
}
