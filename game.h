#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#define MAX 100
#define UP 1
#define RIGHT 2
#define DOWN 3
#define LEFT 4

void initGrid(int,int);
void draw_grid();
void draw_target();
void draw_snake();

#endif
